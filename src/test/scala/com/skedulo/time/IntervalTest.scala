package com.skedulo.time

import com.skedulo.time.IntervalImplicits._
import org.scalacheck.Gen
import org.scalatest.prop.PropertyChecks
import org.scalatest.{Matchers, WordSpec}
import com.skedulo.time.Generators._

class IntervalTest extends WordSpec with Matchers with PropertyChecks {

  final val LARGE_LIST_SIZE = 100000

  "Intervals" should {
    "allow start less than or equal to end" in {
      Interval(1, 2) shouldEqual Interval(1, 2)
      Interval(1, 1) shouldEqual Interval(1, 1)
    }
  }

  "intersects" should {
    "return false when the two intervals do not intersect" in {
      Interval(1, 2) intersects Interval(3, 4) shouldBe false
      Interval(3, 4) intersects Interval(1, 2) shouldBe false
      Instant(1) intersects Interval(1, 2) shouldBe false
      Instant(1) intersects Instant(1) shouldBe false
    }
  }

  it should {
    "return false when the two intervals touch but do not intersect" in {
      Interval(1, 2) intersects Interval(2, 3) shouldBe false
      Interval(2, 3) intersects Interval(1, 2) shouldBe false
      Interval(3, 4) intersects Interval.since(4L) shouldBe false
      Interval(3, 4) intersects Interval.priorTo(3L) shouldBe false
    }
  }

  it should {
    "return true when the two intervals do intersect" in {
      Interval(1, 3) intersects Interval(2, 4) shouldBe true
      Interval(1, 3) intersects Interval(2, 3) shouldBe true
      Interval(1, 3) intersects Interval(1, 3) shouldBe true
      Interval(1, 3) intersects AllOfTime shouldBe true
      Interval(3, 4) touches Interval.since(4L) shouldBe true
      Interval(3, 4) touches Interval.priorTo(3L) shouldBe true
    }
  }

  "overlaps" should {
    "return false when the two intervals do not overlap" in {
      Interval(1, 2) overlaps Interval(3, 4) shouldBe false
      Interval(3, 4) overlaps Interval(1, 2) shouldBe false
    }
  }

  it should {
    "return true when the two intervals touch but do not overlap" in {
      Instant(1) overlaps Interval(1, 2) shouldBe true
      Instant(1) overlaps Instant(1) shouldBe true
      Interval(1, 2) overlaps Interval(2, 3) shouldBe true
      Interval(2, 3) overlaps Interval(1, 2) shouldBe true
      Interval(3, 4) overlaps Interval.since(4L) shouldBe true
      Interval(3, 4) overlaps Interval.priorTo(3L) shouldBe true
    }
  }

  it should {
    "return true when the two intervals do overlap" in {
      Interval(1, 3) overlaps Interval(2, 4) shouldBe true
      Interval(1, 3) overlaps Interval(2, 3) shouldBe true
      Interval(1, 3) overlaps Interval(1, 3) shouldBe true
      Interval(1, 3) overlaps AllOfTime shouldBe true
      Interval(3, 4) touches Interval.since(4L) shouldBe true
      Interval(3, 4) touches Interval.priorTo(3L) shouldBe true
    }
  }

  "touches" should {
    "return true when two intervals touch" in {
      Interval(1, 2) touches Interval(2, 3) shouldBe true
      Interval(2, 3) touches Interval(1, 2) shouldBe true
    }
  }

  it should {
    "return false when two intervals overlap" in {
      Interval(1, 3) touches Interval(2, 4) shouldBe false
      Interval(1, 3) touches Interval(2, 3) shouldBe false
      Interval(1, 3) touches Interval(1, 3) shouldBe false
      Interval(1, 3) touches Interval(1, 10) shouldBe false
      Interval(1, 3) touches AllOfTime shouldBe false
    }
  }

  "length" should {
    "return the correct value for non-empty intervals" in {
      Interval(1, 3).length shouldBe 2
      Interval(3, 5).length shouldBe 2
      Interval(1, 2).length shouldBe 1
    }
  }

  it should {
    "return 0 for empty intervals" in {
      Instant(1).length shouldBe 0
      Interval(1, 1).length shouldBe 0
    }
  }

  it should {
    "return Long.MaxValue for infinite intervals" in {
      Interval.since(1).length shouldBe Long.MaxValue
      Interval.priorTo(1).length shouldBe Long.MaxValue
      AllOfTime.length shouldBe Long.MaxValue
    }
  }

  "includes/includedBy" should {
    "be true if discrete times inside the interval" in {
      Interval(1, 3) includes 1 shouldBe true
      Instant(1) includedBy Interval(1, 3) shouldBe true
      Interval(1, 3) includes 2 shouldBe true
      Instant(2) includedBy Interval(1, 3) shouldBe true
    }
  }

  it should {
    "not contain discrete times outside the interval" in {
      Interval(1, 3) includes 0 shouldBe false
      Instant(0) includedBy Interval(1, 3) shouldBe false
      Interval(1, 3) includes 4 shouldBe false
      Instant(4) includedBy Interval(1, 3) shouldBe false
      Interval(1, 3) includes 3 shouldBe false
      Instant(3) includedBy Interval(1, 3) shouldBe false
    }
  }

  it should {
    "include intervals inside the interval or boundary" in {
      Interval(1, 3) includes Interval(1, 2) shouldBe true
      Interval(1, 3) includes Interval(1, 1) shouldBe true
      Interval(1, 3) includes Interval(1, 3) shouldBe true

      Interval(1, 2) includedBy Interval(1, 3) shouldBe true
      Interval(1, 1) includedBy Interval(1, 3) shouldBe true
      Interval(1, 3) includedBy Interval(1, 3) shouldBe true
    }
  }

  it should {
    "not include intervals that are partially, or wholly in the exterior" in {
      Interval(1, 3) includes Interval(1, 4) shouldBe false
      Interval(3, 4) includes Interval(1, 2) shouldBe false
      Interval(1, 3) includes Interval(4, 6) shouldBe false

      Interval(1, 4) includedBy Interval(1, 3) shouldBe false
      Interval(1, 2) includedBy Interval(3, 4) shouldBe false
      Interval(4, 6) includedBy Interval(1, 3) shouldBe false
    }
  }

  it should {
    "be symmetric" in {
      forAll(genInterval(), genInterval()) { (a: Interval, b: Interval) => {
        a includes b shouldBe (b includedBy a)
      }
      }
    }
  }

  "disjoint" should {
    "return true if intervals have no interior or boundary points in common" in {
      Interval(1, 3) disjoint Interval(4, 5) shouldBe true
      Interval(1, 3) disjoint Interval(3, 4) shouldBe true
      Interval(1, 3) disjoint Interval(2, 4) shouldBe false
    }
  }

  "span" should {
    "produce an interval that covers both of the original intervals" in {
      Interval(1, 3) span Interval(1, 3) shouldBe Interval(1, 3)
      Interval(1, 3) span Interval(1, 1) shouldBe Interval(1, 3)
      Interval(1, 3) span Interval(100, 100) shouldBe Interval(1, 100)
      Interval(50, 100) span Interval(1, 1000) shouldBe Interval(1, 1000)
    }
  }

  "split" should {
    "produce two intervals covered by the original interval" in {
      (1, 3) split 2 shouldBe Some((Interval(1, 2), Interval(2, 3)))
      (1, 3) split 1 shouldBe Some((Interval(1, 1), Interval(1, 3)))
    }
  }

  "split" should {
    "return None if instant not contained in interval" in {
        (1, 3) split 3 shouldBe None
    }
  }

  "centre" should {
    "return centre instant of interval" in {
      (1, 2).centre shouldBe 1
      (1, 3).centre shouldBe 2
      (1, 1).centre shouldBe 1
    }
  }

  it should {
    "return the finite value for an infinite interval" in {
      Interval.since(1).centre shouldBe 1
      Interval.priorTo(1).centre shouldBe 1
    }
  }

  it should {
    "return 0 for AllOfTime" in {
      AllOfTime.centre shouldBe 0
    }
  }

  "before" should {
    "be true if instant is before interval" in {
      Interval(2, 4) before 1 shouldBe false
      Interval(2, 4) before 2 shouldBe false
      Interval(2, 4) before 4 shouldBe true
      Interval(2, 4) before 5 shouldBe true
    }
  }

  "after" should {
    "be true if instant is before interval" in {
      Interval(2, 4).after(1) shouldBe true
      Interval(2, 4).after(2) shouldBe true
      Interval(2, 4).after(4) shouldBe false
      Interval(2, 4).after(5) shouldBe false
    }
  }

  "adjacent discrete intervals" should {
    "be ordered by start time" in {
      val ordered: Seq[Interval] = List((1, 2), (2, 3), (3, 4), (5, 6))
      val unordered: Seq[Interval] = List((5, 6), (2, 3), (1, 2), (3, 4))
      ordered.sorted shouldBe ordered
      unordered.sorted shouldBe ordered
    }
  }

  "disjoint discrete intervals" should {
    "be ordered by start time" in {
      val ordered: Seq[Interval] = List((1, 2), (3, 4), (6, 7), (9, 10))
      val unordered: Seq[Interval] = List((9, 10), (6, 7), (1, 2), (3, 4))
      ordered.sorted shouldBe ordered
      unordered.sorted shouldBe ordered
    }
  }

  "overlapping" should {
    "be ordered by start time and then end time" in {
      val ordered: Seq[Interval] = List((1, 2), (1, 4), (3, 5), (4, 6), (6, 7), (9, 10))
      val unordered: Seq[Interval] = List((1, 4), (3, 5), (1, 2), (6, 7), (9, 10), (4, 6))
      ordered.sorted shouldBe ordered
      unordered.sorted shouldBe ordered
    }
  }

  "diff" should {
    "return the (asymmetric) difference between Intervals" in {
      Interval(1, 2) diff Interval(2, 3) shouldBe IntervalSet(Interval(1, 2))
      Interval(1, 2) diff Interval(1, 2) shouldBe IntervalSet()
      Interval(2, 3) diff Interval(1, 2) shouldBe IntervalSet(Interval(2, 3))
      Interval(1, 5) diff Interval(4, 7) shouldBe IntervalSet(Interval(1, 4))
      Interval(1, 10) diff Interval(2, 9) shouldBe IntervalSet(Interval(1, 2), Interval(9, 10))
      Interval(1, 2) diff Interval(1, 10) shouldBe IntervalSet()
      Interval(2, 10) diff Interval(1, 20) shouldBe IntervalSet()
    }
  }

  "union" should {
    "return the union of two Intervals" in {
      Interval(1, 2) union Interval(2, 3) shouldBe IntervalSet(Interval(1, 3))
      Interval(1, 2) union Interval(3, 4) shouldBe IntervalSet(Interval(1, 2), Interval(3, 4))
      Interval(2, 10) union Interval(5, 6) shouldBe IntervalSet(Interval(2, 10))
    }
  }

  "intersection" should {
    "return the union of two Intersections" in {
      Interval(1, 2) intersection Interval(3, 4) shouldBe IntervalSet()
      Interval(1, 2) intersection Interval(2, 3) shouldBe IntervalSet()
      Interval(1, 3) intersection Interval(2, 4) shouldBe IntervalSet(Interval(2, 3))
    }
  }

  it should {
    "be true only if disjoint is false" in {
      forAll (genInterval(), genInterval()) { (a : Interval, b : Interval) => {
          a intersects b shouldBe !(a disjoint b)
        }
      }
    }
  }

  it should {
    "be symmetric" in {
      forAll (genInterval(), genInterval()) { (a : Interval, b : Interval) => {
        a intersects b shouldBe (b intersects a)
      }
      }
    }
  }

  "complement" should {
    "return the complement of an Interval" in {
      Interval(1, 2).complement shouldBe IntervalSet(Interval.priorTo(1), Interval.since(2))
      Interval.priorTo(1).complement shouldBe IntervalSet(Interval.since(1))
      Interval.since(1).complement shouldBe IntervalSet(Interval.priorTo(1))
    }
  }

  "flatten" should {
    "handle empty interval list" in {
      Intervals.flatten(Nil) shouldBe Nil
    }
  }

  it should {
    "be an identity in the case that no items overlap" in {
      val nonOverlappingOrTouching = List(Interval(1, 2), Interval(4, 5), Interval(6, 7))
      Intervals.flatten(nonOverlappingOrTouching) shouldBe nonOverlappingOrTouching
    }
  }

  it should {
    "flatten wholly overlapping intervals" in {
      Intervals.flatten(List(Interval(1, 10), Interval(2, 3), Interval(5, 7), Interval(2, 3))) shouldBe List(Interval(1, 10))
      Intervals.flatten(List(Interval(1, 7), Interval(2, 3), Interval(5, 7), Interval(2, 3), Interval(1, 10), Interval(20, 30))) shouldBe List(Interval(1, 10), Interval(20, 30))
    }
  }

  it should {
    "flatten partially overlapping intervals" in {
      Intervals.flatten(List(Interval(1, 10), Interval(5, 20))) shouldBe List(Interval(1, 20))
      Intervals.flatten(List(Interval(1, 10), Interval(5, 20), Interval(2, 30), Interval(7, 8), Interval(40, 50))) shouldBe List(Interval(1, 30), Interval(40, 50))
    }
  }

  it should {
    "flatten adjacent non-overlapping intervals" in {
      Intervals.flatten(List(Interval(1, 10), Interval(10, 20))) shouldBe List(Interval(1, 20))
      Intervals.flatten(List(Interval(3, 4), Interval(2, 3), Interval(1, 2))) shouldBe List(Interval(1, 4))
    }
  }

  it should {
    "flatten very large lists" should {
      forAll(genIntervalList(LARGE_LIST_SIZE), minSuccessful(1)) { a => {
        val span = Intervals.span(a)
        val flattened = Intervals.flatten(a)
        flattened.forall(span.contains(_))
      }
      }
    }
  }

  "diff" should {
    "handle empty interval lists" in {
      Intervals.diff(Nil, Nil) shouldBe Nil
      Intervals.diff(List(Interval(1, 2)), Nil) shouldBe List(Interval(1, 2))
      Intervals.diff(Nil, List(Interval(1, 2))) shouldBe Nil
    }
  }

  it should {
    "handle non-overlapping lists" in {
      Intervals.diff(List(Interval(1, 2), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(1, 2), Interval(10, 15), Interval(30, 40))

    }
  }

  it should {
    "handle overlapping lists" in {
      // Multiple overlaps
      Intervals.diff(
        List(Interval(1, 20), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(1, 5), Interval(6, 15), Interval(30, 40))

      // a contains b
      Intervals.diff(List(Interval(1, 20)), List(Interval(5, 10))) shouldBe List(Interval(1, 5), Interval(10, 20))

      // a contains b with multiple bs
      Intervals.diff(List(Interval(1, 20)), List(Interval(5, 10), Interval(11, 15))) shouldBe List(Interval(1, 5), Interval(10, 11), Interval(15, 20))

      // left overlap
      Intervals.diff(List(Interval(1, 20)), List(Interval(10, 40))) shouldBe List(Interval(1, 10))

      // right overlap
      Intervals.diff(List(Interval(10, 40)), List(Interval(1, 20))) shouldBe List(Interval(20, 40))
    }
  }

  "intersection" should {
    "handle empty interval lists" in {
      Intervals.intersection(Nil, Nil) shouldBe Nil
      Intervals.intersection(List(Interval(1, 2)), Nil) shouldBe Nil
      Intervals.intersection(Nil, List(Interval(1, 2))) shouldBe Nil
    }
  }

  it should {
    "handle non-overlapping lists" in {
      Intervals.intersection(
        List(Interval(1, 2), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(15, 20))

    }
  }

  it should {
    "handle overlapping lists" in {
      // Multiple overlaps
      Intervals.intersection(
        List(Interval(1, 20), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(5, 6), Interval(15, 20))

      // a contains b
      Intervals.intersection(List(Interval(1, 20)), List(Interval(5, 10))) shouldBe List(Interval(5, 10))

      // a contains b with multiple bs
      Intervals.intersection(List(Interval(1, 20)), List(Interval(5, 10), Interval(11, 15))) shouldBe List(Interval(5,10), Interval(11,15))

      // left overlap
      Intervals.intersection(List(Interval(1, 20)), List(Interval(10, 40))) shouldBe List(Interval(10, 20))

      // right overlap
      Intervals.intersection(List(Interval(10, 40)), List(Interval(1, 20))) shouldBe List(Interval(10, 20))
    }
  }

  it should {
    "handle large lists" in {
      forAll(genIntervalList(LARGE_LIST_SIZE), genIntervalList(LARGE_LIST_SIZE), minSuccessful(1)) { (a, b) =>
        Intervals.intersection(a, b) shouldBe Intervals.intersection(b, a)
      }
    }
  }

  it should {
    "be symmetric for lists" in {
      forAll (genIntervalList(), genIntervalList()) { (a, b) => {
        Intervals.intersection(a, b) shouldBe Intervals.intersection(b, a)
      }
      }
    }
  }

  "union" should {
    "handle empty interval lists" in {
      Intervals.union(Nil, Nil) shouldBe Nil
      Intervals.union(List(Interval(1, 2)), Nil) shouldBe List(Interval(1, 2))
      Intervals.union(Nil, List(Interval(1, 2))) shouldBe List(Interval(1, 2))
    }
  }

  it should {
    "handle non-overlapping lists" in {
      Intervals.union(
        List(Interval(1, 2), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(1, 2), Interval(5, 6), Interval(10, 20), Interval(30, 40), Interval(45, 50))
    }
  }

  it should {
    "handle overlapping lists" in {
      // Multiple overlaps
      Intervals.union(
        List(Interval(1, 20), Interval(10, 20), Interval(30, 40)),
        List(Interval(5, 6), Interval(15, 20), Interval(45, 50))) shouldBe
        List(Interval(1, 20), Interval(30, 40), Interval(45, 50))

      // a contains b
      Intervals.union(List(Interval(1, 20)), List(Interval(5, 10))) shouldBe List(Interval(1, 20))

      // a contains b with multiple bs
      Intervals.union(List(Interval(1, 20)), List(Interval(5, 10), Interval(11, 15))) shouldBe List(Interval(1, 20))

      // left overlap
      Intervals.union(List(Interval(1, 20)), List(Interval(10, 40))) shouldBe List(Interval(1, 40))

      // right overlap
      Intervals.union(List(Interval(10, 40)), List(Interval(1, 20))) shouldBe List(Interval(1, 40))
    }
  }

  it should {
    "handle large lists" in {
      forAll(genIntervalList(LARGE_LIST_SIZE), genIntervalList(LARGE_LIST_SIZE), minSuccessful(1)) { (a, b) =>
        Intervals.union(a, b) shouldBe Intervals.union(b, a)
      }
    }
  }

  it should {
    "be symmetric for lists" in {
      forAll (genIntervalList(), genIntervalList()) { (a, b) => {
        Intervals.union(a, b) shouldBe Intervals.union(b, a)
      }
      }
    }
  }

  "merge operations" should {
    "handle large lists" in {
      forAll (genIntervalList(LARGE_LIST_SIZE), genIntervalList(LARGE_LIST_SIZE), minSuccessful(1)) { (a, b) => {
        Intervals.intersection(Intervals.diff(a, b), Intervals.intersection(a, b)) shouldBe Nil
        Intervals.intersection(a, Intervals.union(a, b)) shouldBe Intervals.flatten(a)
      }
      }
    }
  }

  "selfIntersection" should {
    "handle empty lists" in {
      Intervals.selfIntersection(Nil) shouldBe Nil
      Intervals.selfIntersecting(Nil) shouldBe false
    }
  }

  it should {
    "handle disjoint lists" in {
      val disjointIntervals = List(Interval(1, 2), Interval(3, 4), Interval(4, 5))
      Intervals.selfIntersection(disjointIntervals) shouldBe Nil
      Intervals.selfIntersecting(disjointIntervals) shouldBe false
    }
  }

  it should {
    "handle self-intersecting lists" in {
      val selfIntersectingList = List(Interval(1, 3), Interval(2, 4), Interval(3, 5))
      Intervals.selfIntersection(selfIntersectingList) shouldBe List(Interval(1, 5))
      Intervals.selfIntersecting(selfIntersectingList) shouldBe true

      // Multiple intersections
      val multipleIntersections = List(Interval(1, 20), Interval(10, 20), Interval(30, 40), Interval(5, 6), Interval(15, 20), Interval(45, 50))
      Intervals.selfIntersection(multipleIntersections) shouldBe
        List(Interval(1, 20))
      Intervals.selfIntersecting(multipleIntersections) shouldBe true

      // a contains b
      val aIncludesB = List(Interval(1, 20), Interval(5, 10))
      Intervals.selfIntersection(aIncludesB) shouldBe List(Interval(1, 20))
      Intervals.selfIntersecting(aIncludesB) shouldBe true

      // a contains b with multiple bs
      val aIncludesMultipleBs = List(Interval(1, 20), Interval(5, 10), Interval(11, 15))
      Intervals.selfIntersection(aIncludesMultipleBs) shouldBe List(Interval(1, 20))
      Intervals.selfIntersecting(aIncludesMultipleBs) shouldBe true

      // left overlap
      val leftOverlap = List(Interval(1, 20), Interval(10, 40))
      Intervals.selfIntersection(leftOverlap) shouldBe List(Interval(1, 40))
      Intervals.selfIntersecting(leftOverlap) shouldBe true

      // right overlap
      val rightOverlap = List(Interval(10, 40), Interval(1, 20))
      Intervals.selfIntersection(rightOverlap) shouldBe List(Interval(1, 40))
      Intervals.selfIntersecting(rightOverlap)
    }
  }

  it should {
    "All self intersecting intervals should intersect with more than one interval in the IntervalSet of those intervals" in {
      forAll (genIntervalList()) { (a) => {
        val intervalSet : IntervalSet = IntervalSet(a)
        Intervals.selfIntersection(a).forall(intervalSet.intersection(_).toSet.size > 1)
      }
      }
    }
  }

}

