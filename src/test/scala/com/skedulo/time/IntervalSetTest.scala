package com.skedulo.time

import com.skedulo.time.Generators.genIntervalList
import com.skedulo.time.IntervalImplicits._
import org.scalatest.prop.PropertyChecks
import org.scalatest.{Matchers, WordSpec}

class IntervalSetTest extends WordSpec with Matchers with PropertyChecks {
  private val discreteAdjacentIntervals = IntervalSet(Interval(1, 2), Interval(2, 3), Interval(3, 4), Interval(4, 5), Interval(5, 6), Interval(6, 7))
  private val overlappingIntervals = IntervalSet(Interval(1, 5), Interval(7, 10), Interval(1, 20), Interval(21, 100))

  "IntervalSet" should {
    "create large sets" in {
        var n : Int = 0
        var sum : Double = 0
        forAll (genIntervalList(1000000), minSize(1)) { intervals =>
          val start = System.nanoTime()
          val intervalSet = IntervalSet(intervals)
          val end = System.nanoTime()
          sum += (end - start) / 1e6
          n += 1
        }
        printf("[%d x] Create IntervalSet for 1M items = %fms\n", n, sum / n)
      }
  }

  "Empty IntervalTree" should {
    "return overlaps for individual intervals" in {
      IntervalSet(Nil).intersection((1, 2)) shouldBe IntervalSet()
    }
  }

  "Discrete Adjacent IntervalTree" should {
    "return overlaps for individual intervals" in {
      discreteAdjacentIntervals.intersection((1, 8)) shouldBe discreteAdjacentIntervals
      discreteAdjacentIntervals.intersection((3, 4)) shouldBe IntervalSet(3, 4)
      discreteAdjacentIntervals.intersection((1, 2)) shouldBe IntervalSet(1, 2)
      discreteAdjacentIntervals.intersection((5, 6)) shouldBe IntervalSet(5, 6)
      discreteAdjacentIntervals.intersection((6, 7)) shouldBe IntervalSet(6, 7)
      discreteAdjacentIntervals.intersection((2, 6)) shouldBe IntervalSet(Interval(2, 3), Interval(3, 4), Interval(4, 5), Interval(5, 6))
      discreteAdjacentIntervals.intersection((100, 101)) shouldBe IntervalSet()
    }
  }

  "intersection IntervalTree" should {
    "return overlaps for individual intervals" in {
      overlappingIntervals.intersection((1, 100)) shouldBe overlappingIntervals
      overlappingIntervals.intersection((1, 5)) shouldBe IntervalSet(Interval(1, 5), Interval(1, 20))
      overlappingIntervals.intersection((101, 102)) shouldBe IntervalSet()
    }
  }

  //it should {
  //  "return overlaps with other interval trees" in {
  //    overlappingIntervals.intersection(IntervalSet()) shouldBe IntervalSet()
  //    overlappingIntervals.intersection(overlappingIntervals) shouldBe overlappingIntervals
  //  }
  //}

  "union" should {
    "be associative" in {
      val a = IntervalSet(Interval(1, 2))
      val b = IntervalSet(Interval(2, 3))
      val c = IntervalSet(Interval(3, 4))
      val expected = IntervalSet(Interval(1, 2), Interval(2, 3), Interval(3, 4))
      ((a union b) union c) shouldBe expected
      (a union (b union c)) shouldBe expected
      a union c union b shouldBe expected
    }
  }

  "union" should {
    "be NOOP with self" in {
      val a = discreteAdjacentIntervals
      a union a shouldBe a
      // TODO: The optimisation => a union a eq a shouldBe true
    }
  }

  it should {
    "combine different IntervalSets" in {
      IntervalSet() union IntervalSet() shouldBe IntervalSet()
      IntervalSet() union discreteAdjacentIntervals shouldBe discreteAdjacentIntervals
      discreteAdjacentIntervals union IntervalSet(Nil) shouldBe discreteAdjacentIntervals
      discreteAdjacentIntervals union discreteAdjacentIntervals shouldBe discreteAdjacentIntervals
      discreteAdjacentIntervals union IntervalSet(Interval(8, 9)) shouldBe IntervalSet(Interval(1, 2), Interval(2, 3),
        Interval(3, 4), Interval(4, 5), Interval(5, 6), Interval(6, 7), Interval(8, 9))
      (IntervalSet(Interval(1, 2)) union IntervalSet(Interval(2, 3))) union IntervalSet(Interval(3, 4)) union
        IntervalSet(Interval(4, 5)) union IntervalSet(Interval(5, 6)) union
        IntervalSet(Interval(6, 7)) shouldBe discreteAdjacentIntervals
    }
  }

  "overlapping" should {
    "handle empty lists" in {
      IntervalSet().intersection(Nil) shouldBe Nil
    }
  }

  it should {
    "handle non-overlapping lists" in {
      IntervalSet(List(Interval(1, 2), Interval(10, 20), Interval(30, 40)))
        .intersection(IntervalSet(List(Interval(5, 6), Interval(15, 20), Interval(45, 50)))) shouldBe List(Interval(10, 20))
    }
  }

  "selfIntersection" should {
    "handle empty lists" in {
      IntervalSet().selfIntersection() shouldBe Nil
      IntervalSet().selfIntersecting() shouldBe false
    }
  }

  it should {
    "return empty for adjacent non-selfIntersection lists" in {
      IntervalSet(List(Interval(1, 2), Interval(2, 3), Interval(3, 4))).selfIntersection() shouldBe Nil
    }
  }

  it should {
    "handle non selfIntersection lists" in {
      IntervalSet(List(Interval(1, 2), Interval(3, 4), Interval(4, 5))).selfIntersection() shouldBe Nil
    }
  }

  it should {
    "handle selfIntersection lists" in {
      IntervalSet(List(Interval(1, 3), Interval(2, 4), Interval(3, 5))).selfIntersection() shouldBe List(Interval(1, 3), Interval(2, 4), Interval(3, 5))

      // Multiple overlaps
      IntervalSet(List(Interval(1, 20), Interval(10, 20), Interval(30, 40), Interval(5, 6), Interval(15, 20), Interval(45, 50))).selfIntersection() shouldBe
        List(Interval(5,6), Interval(1,20), Interval(10,20), Interval(15,20))

      // a contains b
      IntervalSet(List(Interval(1, 20), Interval(5, 10))).selfIntersection() shouldBe List(Interval(1, 20), Interval(5, 10))

      // a contains b with multiple bs
      IntervalSet(List(Interval(1, 20), Interval(5, 10), Interval(11, 15))).selfIntersection() shouldBe List(Interval(1, 20), Interval(5, 10), Interval(11, 15))

      // left overlap
      IntervalSet(List(Interval(1, 20), Interval(10, 40))).selfIntersection() shouldBe List(Interval(1, 20), Interval(10, 40))

      // right overlap
      IntervalSet(List(Interval(1, 20), Interval(10, 40))).selfIntersection() shouldBe List(Interval(1, 20), Interval(10, 40))
    }
  }

}
