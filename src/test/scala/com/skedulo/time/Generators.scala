package com.skedulo.time

import org.scalacheck.Gen

object Generators {
  def genInterval(instantGen: Gen[Long] = Gen.posNum[Long], lengthGen : Gen[Long] = Gen.choose(3600000, 8 * 3600000)): Gen[Interval] = {
    for {
      start <- instantGen
      duration <- Gen.posNum[Int]
    } yield Interval(start, start + duration)
  }

  def genIntervalList(minSize : Int = 0, maxSize : Int = 100, intervalGenerator : Gen[Interval] = genInterval()) : Gen[List[Interval]] = {
    for {
      listSize <- Gen.choose(minSize, Math.max(minSize, maxSize))
      list <- Gen.listOfN(listSize, intervalGenerator)
    } yield list
  }

}
