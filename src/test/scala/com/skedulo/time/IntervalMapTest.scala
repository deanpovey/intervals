package com.skedulo.time

import com.skedulo.time.Generators.genIntervalList
import com.skedulo.time.Intervals.diff
import org.scalatest.prop.PropertyChecks
import org.scalatest.{Matchers, WordSpec}

class IntervalMapTest extends WordSpec with Matchers with PropertyChecks {
  "IntervalMap" should {
    "create empty map" in {
      val map = IntervalMap[String]()
      map(Interval(1, 10)) shouldBe Set()
      map.toMap() shouldBe Map()
      map.toIntervalSet() shouldBe IntervalSet()
      map.toInverseMap() shouldBe Map()

    }
  }

  it should {
    "allow a single item" in {
      val string = "Hello World!"
      val singleItemMap = Map(Interval(1, 10) -> List(string))
      val map = new IntervalMap(singleItemMap)
      map(Interval(1, 10)) shouldBe Set(string)
      map(Interval(2, 5)) shouldBe Set(string)
      map.intervals(string) shouldBe Set(Interval(1, 10))
    }
  }

  it should {
    "create large maps" in {
      var n : Int = 0
      var sum : Double = 0
      forAll (genIntervalList(1000000), minSize(1)) { jobIntervals =>
        val jobs = toJobs(jobIntervals)
        val start = System.nanoTime()
        val intervalMap = IntervalMap(jobs)
        val end = System.nanoTime()
        sum += (end - start) / 1e6
        n += 1
      }
      printf("[%d x] Create IntervalMap for 1M items = %fms\n", n, sum / n)
    }
  }


  // Use cases
  case class Job(jobId : Int)

  def findGaps(interval : Interval, jobs : IntervalMap[Job]) : Iterable[Interval] = {
    diff(List(interval), jobs.toIntervalSet())
  }

  def findGaps(interval : Interval, jobs : IntervalMap[Job], availability : Iterable[Interval]) : Iterable[Interval] = {
    diff(IntervalSet(availability).intersection(interval), jobs.toIntervalSet())
  }

  def findSchedulingConflicts(jobs : IntervalMap[Job]) : Iterable[Job] = {
    jobs.toIntervalSet().selfIntersection().flatMap(jobs.toMap()(_))
  }

  def eitherSideFatigueRule(jobs : IntervalSet) : IntervalSet = {
    // At least 1 before and after a job
    IntervalSet(jobs.map { case Interval(start, end) => Interval(start - 1, end + 1) })
  }

  def atLeast5AfterOddJobs(jobs : IntervalMap[Job]) : IntervalSet = {
    val oddJobs = jobs.toInverseMap().keys.filter(job => job.jobId % 2 != 0)
    val intervalsOf5AfterOddJobs = oddJobs.flatMap {
      job => jobs.toInverseMap()(job).map {
        case Interval(_, finish) => Interval(finish, finish + 5)
      }
    }
    IntervalSet(intervalsOf5AfterOddJobs)
  }

  val jobs = IntervalMap(Map(
    Interval(1, 12) -> Set(Job(0)),
    Interval(17, 21) -> Set(Job(1)),
    Interval(25, 35) -> Set(Job(2)),
    Interval(40, 47) -> Set(Job(3))
  ))

  val jobsWithSchedulingConflicts = IntervalMap(Map(
    Interval(1, 12) -> Set(Job(0)),
    Interval(17, 27) -> Set(Job(1)),
    Interval(25, 35) -> Set(Job(2)),
    Interval(40, 47) -> Set(Job(3))
  ))


  val availability = IntervalSet(Interval(1, 15), Interval(16, 38), Interval(40, 50))


  "Find gaps" should {
    "find gaps in job schedule" in {
      findGaps(Interval(1, 60), jobs) shouldBe List(Interval(12,17), Interval(21,25), Interval(35,40), Interval(47,60))
      findGaps(Interval(1, 60), jobs) filter { _.length >= 5 } shouldBe List(Interval(12, 17), Interval(35, 40), Interval(47, 60))
    }
  }

  it should {
    "find gaps with availability" in {
      findGaps(Interval(1, 60), jobs, availability).toList shouldBe List(Interval(12,15), Interval(16,17), Interval(21,25), Interval(35,38), Interval(47,50))
    }
  }

  it should {
    "find gaps with availability determined by fatigue rule" in {
      findGaps(Interval(1, 60), jobs, diff(availability, eitherSideFatigueRule(jobs.toIntervalSet()))).toList shouldBe List(Interval(13,15), Interval(22,24), Interval(36,38), Interval(48,50))

    }
  }

  it should {
    "find gaps with availability determined by arbitrary job rule" in {
      val fatigue = atLeast5AfterOddJobs(jobs)
      fatigue shouldBe IntervalSet(Interval(21,26), Interval(47,52))
      findGaps(Interval(1, 60), jobs, diff(availability, fatigue)).toList shouldBe List(Interval(12,15), Interval(16,17), Interval(35,38))
    }
  }

  "findSchedulingConflicts" should {
    "return empty for jobs with no conflicts" in {
      findSchedulingConflicts(jobs).isEmpty shouldBe true
    }
  }

  it should {
    "return conflicting jobs where conflicts exist" in {
      findSchedulingConflicts(jobsWithSchedulingConflicts) shouldBe List(Job(1), Job(2))
    }
  }



  it should {
    "find conflicts for large schedules" in {
      var n : Int = 0
      var sum : Double = 0
      forAll (genIntervalList(1000000), minSize(1)) { jobIntervals =>
        val jobsIntervalMap: IntervalMap[Job] = toJobsIntervalMap(jobIntervals)
        val start = System.nanoTime()
        findSchedulingConflicts(jobsIntervalMap)
        val end = System.nanoTime()
        sum += (end - start) / 1e6
        n += 1
      }
      printf("[%d x] Find conflicts for 1M items = %fms\n", n, sum / n)
    }
  }

  it should {
    "find gaps for large schedules" in {
      var n : Int = 0
      var sum : Double = 0
      forAll (genIntervalList(1000000), genIntervalList(1000000), minSize(1)) { (jobIntervals, availability) =>
        val jobsIntervalMap: IntervalMap[Job] = toJobsIntervalMap(jobIntervals)
        val start = System.nanoTime()
        //jobsIntervalMap.toIntervalSet().size shouldBe 10000000
        findGaps(AllOfTime, jobsIntervalMap, availability)
        val end = System.nanoTime()
        sum += (end - start) / 1e6
        n += 1
      }
      printf("[%d x] Find gaps for 1M items = %fms\n", n, sum / n)
    }
  }


  private def toJobsIntervalMap(jobIntervals: List[Interval]) = {
    val jobs: Map[Interval, List[Job]] = toJobs(jobIntervals)
    val jobsIntervalMap = IntervalMap(jobs)
    jobsIntervalMap
  }

  private def toJobs(jobIntervals: List[Interval]) = {
    val jobIds = 0 to jobIntervals.size
    val jobs = jobIntervals
      .zipWithIndex
      .map { case (i, jobId) => i -> Job(jobId) }
      .groupBy(_._1).map { case (k, v) => (k, v.map(_._2)) }
    jobs
  }
}
