package com.skedulo.time

import com.skedulo.time.IntervalImplicits._

import scala.collection.immutable.Nil
import scala.language.implicitConversions

/**
  * An interval represents a time period. It is half-open - ie. mathematically:
  *
  * <code><pre>
  *   Interval(start, end) = {x | start <= x < end}.
  * </pre></code>
  *
  * <p>A zero length interval (e.g. (1, 1)) is considered an instant.
  *
  * <p>The concept of an empty interval is not supported, however these can represented by an empty interval set.</p>
  *
  * <p>(Practically) Infinite intervals are supported for negative infinity (Long.MIN_VALUE) and positive infinity
  * (Long.MAX_VALUE).</p>
  *
  * <p>It is worth noting that the start point is inside the interval, whereas the end point is not.  This corresponds
  * to the general concept of ranges which are defined half-open, (mathematically closed, open [start, end)).</p>
  *
  * <p>Intervals support a number of predicates:
  * <ul>
  *   <li>intersects - True if the intervals have a point inside the interval in common
  *   <li>overlaps - Similar to intersects, but also true if the only point in common is the boundary (ie.
  *   (1, 5) overlaps (5, 6) == true, but (1, 5) intersects (5, 6) == false
  *   <li>touches - True if the intervals only have a boundary in common (e.g. (1, 5) touches (5, 6)
  *   <li>includes - True if the first interval wholly contains the second interval.  Note: this is always false if
  *   the first interval is an instant.
  * </ul>
  * </p>
  *
  * <p>They also support a number of set-theoretic operations.  All of these return an `IntervalSet` that allows
  * for empty or multiple overlapping or disjoint intervals.
  * <ul>
  *   <li>a intersection b - Returns interval covering all the points in a that are also in b.
  *   <li>a union b - Returns both intervals as a combined set.  Note: if the intervals are touching they are combined into
  *   a single interval.
  *   <li>a diff b - (difference or relative complement) all the points in a that are not in b.
  *   <li>a symmetricDiff b - all the points in a or b that aren't in both (equivalent to (a diff b) union (b diff a)
  *   <li>a complement - All the points that are not in a.  If a is a finite interval this will be an infinite interval
  *   and vice versa,
  *</ul>
  * @param start starting time
  * @param end end time
  */
case class Interval(start: Long, end: Long) extends Ordered[Interval] {
  require(end >= start)

  /**
    * Return the length of this interval or Long.MaxValue if the length is infinite.
    *
    * @return the length of the interval
    */
  def length: Long = _length
  private lazy val _length: Long = if (!infinite) end - start else Long.MaxValue

  /**
    * Intervals are considered intersecting if the start or end of one interval is contained within the other interval.
    *
    * <p>Note: the intervals (1, 5) and (5, 1) are not overlapping but are touching. Also the intersection of any
    * Interval with an Instant is empty (since instants are zero length intervals).</p>
    *
    * <p>a intersects b <-> !a disjoint b</p>
    *
    * @param that interval to intersect
    * @return true if interval intersects this one, false otherwise
    */
  def intersects(that: Interval): Boolean = {
    this.start < that.end && this.end > that.start
  }

  /**
   * Intervals are considered touching if they intersect only at the boundaries.
   *
   * @param that interval to consider
   * @return true if interval touches this one, false otherwise
   */
  def touches(that: Interval): Boolean =
    this.start == that.end || this.end == that.start && !intersects(that)

  /**
   * Whether the interval contains the instant.  Note an Interval does not contain its end point.
   *
   * Example: the interval (1, 3) contains 1 and 2, but not 3 nor 4.
   *
   * @param instant instant to consider
   *
   * @return true if the instant is included within this interval.
   */
  def includes(instant: Long): Boolean = instant >= start && instant < end

  /**
   * An interval includes another interval if it has no exterior points outside the interval.
   *
   * <p>Example: the interval (1, 3) includes (1, 2) and (1, 3) but not (1, 4).  It also includes (1, 1) and (2, 2) but
    * not (3, 3)</p>
   *
   * @param that interval to consider
   *
   * @return true if the interval is covered by this interval
   */
  def includes(that: Interval): Boolean = includes(that.start) && that.end <= this.end

  /**
    * Return true if this interval is included by the required interval.
    *
    * @param that interval to test
    * @return true if supplied interval contains this interval
    */
  def includedBy(that: Interval) : Boolean = that includes this

  /**
    * An overlapping instant is one that is either contained by or touches this interval.
    *
    * <p>Unlike intersects, boundary intersections are allowed, e.g. 3 overlaps (1, 3).
    *
    * @param instant instant to test
    *
    * @return true if the instant overlaps the interior or boundary of this interval.
    */
  def intersects(instant: Long) : Boolean = instant >= this.start && instant <= this.end

  /**
    * An overlapping interval is one that either intersects or touches this interval.
    *
    * <p>Unlike intersects, boundary intersections are allowed, e.g. (1, 3) overlaps (3, 4).
    *
    * @param that interval to test
    *
    * @return true if the interval overlaps the interior or boundary of this interval.
    */
  def overlaps(that: Interval) : Boolean = this.start <= that.end && this.end >= that.start

  /**
   * Two intervals are disjoint if neither has any points in common (they are not overlapping or touching)
   *
   * <p>Example: the intervals (1, 3), (3, 4) and (4, 5) are disjoint, the intervals (1, 3) and and (2, 4)
   * are not.</p>
   *
   * @param that interval to consider
   * @return true if the interval is disjoint with this interval
   */
  def disjoint(that: Interval): Boolean = this.start >= that.end || this.end <= that.start

  /**
   * Return the maximum span of this interval with the supplied interval (ie. the Interval that covers both intervals
   *
   * @param interval interval to consider
   *
   * @return interval for which the covers predicate holds true for both this interval and the supplied one.
   */
  def span(interval: Interval): Interval = (Math.min(this.start, interval.start), Math.max(this.end, interval.end))

  /**
   * Split the interval at the given instant.
   *
   * <p>This method will fail if the instant is not contained by the interval
   *
   * <p>Example: (1, 3).split(2) yields (1, 2) and (2, 3), (1, 3).split(3) will throw an exception. (1, 3).split(1)
   * will yield (1, 1), (1, 3)</p>
   *
   * @param instant instant to split at
   * @return tuple containing each interval
   */
  def split(instant: Long): Option[(Interval, Interval)] = {
    if (this.includes(instant))
      Some((
        Interval(this.start, instant),
        Interval(instant, this.end)
      ))
    else
      None
  }

  /**
   * Return the set-theoretic difference of this Interval with the given interval.
   *
   * @param that interval to find the difference for
   * @return IntervalSet containing intervals in this interval that are not in the given interval
   */
  def diff(that: Interval): IntervalSet = {
    if (disjoint(that)) IntervalSet(this)
    else {
      // For a non-disjoint pair of intervals the following matrix shows the difference
      //
      //         +---------+---------+----------------+
      //         | E1 < E2 | E1 = E2 | E1 > E2        |
      //         +------------------------------------+
      // S1 < S2 |(S1, S2) |(S1, S2) |(S1, S2)(E2, E1)|
      //         +---------+---------+----------------+
      // S1 = S2 |Nil      |Nil      |(E2, E1)        |
      //         +---------+---------+----------------+
      // S1 > S2 |Nil      |Nil      |(E2, E1)        |
      //         +---------+---------+----------------+
      val s1 = this.start
      val s2 = that.start
      val e1 = this.end
      val e2 = that.end
      val initialResult = if (s1 < s2) List(Interval(s1, s2)) else Nil
      if (e1 > e2) IntervalSet(initialResult :+ Interval(e2, e1)) else IntervalSet(initialResult)
    }
  }

  /**
    * Return the symmetric difference of this Interval with the given interval, ie. the elements that are in
    * either interval but not both.
    * @param that Interval to find symmetric difference for
    * @return IntervalSet containing the Symmetric difference
    */
  def symmetricDiff(that: Interval) : IntervalSet = this.diff(that) union that.diff(this)

  /**
    * Return the union of the two intervals, ie. all the elements in this interval and the given interval.
    *
    * @param that Interval to find union with
    *
    * @return union of the two intervals
    */
  def union(that: Interval): IntervalSet = {
    if (touches(that)) IntervalSet(Interval(Math.min(this.start, that.start), Math.max(this.end, that.end)))
    else if (disjoint(that)) IntervalSet(this, that)
    else {
      IntervalSet(Interval(Math.min(this.start, that.start), Math.max(this.end, that.end)))
    }
  }

  /**
    * Return the intersection of two intervals,
    * @param that Interval to intersect with
    * @return Intervals in both this and that
    */
  def intersection(that: Interval) : IntervalSet = {
    if (disjoint(that)) IntervalSet()
    else IntervalSet(Interval(Math.max(this.start, that.start), Math.min(this.end, that.end)))
  }

  /**
    * Return the complement of this Interval. If the Interval is a finite Interval, then the result will be
    * infinite and vice versa.
    *
    * @return IntervalSet containing all possible Intervals not included by this Interval.
    */
  def complement : IntervalSet = _complement
  def ~ : IntervalSet = complement
  private lazy val _complement : IntervalSet = {
    if (this == AllOfTime) IntervalSet()
    else if (this.start == Long.MinValue) IntervalSet(Interval.since(this.end))
    else if (this.end == Long.MaxValue) IntervalSet(Interval.priorTo(this.start))
    else IntervalSet(Interval.priorTo(this.start), Interval.since(this.end))
  }



  /**
    * Convert the Interval to a list of endpoints
    *
    * @return List of Start, End
    */
  def flatten: Iterable[IntervalEndPoint] = _flatten
  private lazy val _flatten: Iterable[IntervalEndPoint] = List(Start(start), End(end))

  /**
   * Return the instant which is at the centre of this interval.
   *
   * <p>Example: (1, 2).centre is 1; (1, 3).centre is 2; (1, 1).centre is 1.<p>
    *
    * <p>The special cases of returning the centre of infinite intervals are handled by returning the non-infinite boundary
    * or zero in the case of `AllOfTime`.
   *
   * @return the centre point of this interval.
   */
  def centre: Long = _centre
  private lazy val _centre: Long = this match {
    case Interval(Long.MinValue, Long.MaxValue) => 0
    case Interval(Long.MinValue, e)             => e
    case Interval(s, Long.MaxValue)             => s
    case Interval(s, e)                         => s + (e - s) / 2
  }

  /**
   * Return whether the interval is before the given instant
   *
   * @param instant instant to consider
   *
   * @return true if the end time is <= this instant
   */
  def before(instant: Long): Boolean = end <= instant

  /**
   * Return whether the interval is after the given instant
   *
   * @param instant instant to consider
   *
   * @return true if the start time is > this instant
   */
  def after(instant: Long): Boolean = start >= instant

  /**
   * Returns true if interval is effectively infinite.
   *
   * @return true if interval is inifinite.
   */
  def infinite: Boolean = _infinite
  private lazy val _infinite: Boolean = start == Long.MinValue || end == Long.MaxValue

  /**
    * Default ordering of Intervals.  We order first by start point and then by end point
    * @param that Interval to compare to
    * @return comparision of two Intervals
    */
  override def compare(that: Interval): Int = {
    val startCompare = this.start.compareTo(that.start)
    if (startCompare == 0) this.end.compareTo(that.end) else startCompare
  }
}

/**
  * Represents an instant in time as a zero length Interval
  * @param instant time to crate Instant for
  */
class Instant(instant: Long) extends Interval(instant, instant)
object Instant {
  def apply(instant: Long): Instant = new Instant(instant)
}

/**
  * Implicit conversions for Intervals
  */
object IntervalImplicits {
  implicit def IntervalTuple(interval: Interval): (Long, Long) = (interval.start, interval.end)
  implicit def Tuple2Interval(tuple: Tuple2[Long, Long]): Interval = new Interval(tuple._1, tuple._2)
  // This would not be used in production but simplifies testing
  implicit def IntervalTuple(tuple: Tuple2[Int, Int]): Interval = new Interval(tuple._1, tuple._2)
  implicit def Long2Instant(instant: Long): Interval = Instant(instant)
}

/**
  * An Interval end point.  Allows tagging of time as the start or end of an Interval.
  */
sealed trait IntervalEndPoint {
  val value : Long
}
case class Start(value : Long) extends IntervalEndPoint
case class End(value : Long) extends IntervalEndPoint
case class OutOfRange(value : Long) extends IntervalEndPoint

object IntervalEndPoint {
  implicit def orderingByValue[A <: IntervalEndPoint]: Ordering[IntervalEndPoint] =
    Ordering.by(_.value)
}

object Interval {
  /**
    * Create an infinite interval encompassing any time since the given instant.
    *
    * @param instant Instant to start from
    *
    * @return an interval from instant to the end of time.
    */
  def since(instant: Long) = Interval(instant, Long.MaxValue)

  /**
    * Create an infinite interval encompassing any time prior to the given instant.
    *
    * @param instant Instant to start from
    *
    * @return an interval from the beginning of time up until the given instant
    */
  def priorTo(instant: Long) = Interval(Long.MinValue, instant)
}

/**
  * The infinite Interval that represents all possible time values.
  */
object AllOfTime extends Interval(Long.MinValue, Long.MaxValue)