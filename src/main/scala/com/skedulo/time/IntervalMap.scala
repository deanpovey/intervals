package com.skedulo.time


/**
  * A bidirectional mapping between Intervals and some other type.
  * @tparam T Type of data to map to
  */
case class IntervalMap[T](items : Map[Interval, Iterable[T]]) {

  private val intervalSet = IntervalSet(items.keys)

  // Note this method of computing the reverse map is very expensive and should be revisited.
  private lazy val reverseMap : Map[T, Set[Interval]] =
    items.flatMap { case (interval, value) => value.map(v => (v, interval)) }
      .groupBy(_._1)
      .map {
        case (k, v) => (k, v.values.toSet)
      }

  /**
    * Return the Intervals that form the keys for this map as an IntervalSet.
    * @return an IntervalSet containing the keys
    */
  def toIntervalSet() = intervalSet

  /**
    * Return as a regular map from Interval -> Iterable[T]
    * @return regular map
    */
  def toMap() = items

  /**
    * Return the inverted map (from type to Set of Intervals).
    * @return inverted map
    */
  def toInverseMap() = reverseMap

  /**
    * Find any values that intersect the request value.
    * @param interval Interval to lookup
    * @return the Set of values whose interval intersects the requested interval or empty set if not items matches
    */
  def apply(interval : Interval) : Set[T] = intervalSet.intersection(interval).flatMap(items(_)).toSet

  /**
    * Return the intervals that are mapped to the given item.
    * @param item item to lookup
    * @return Set of intervals matched or empty set if no items matched.
    */
  def intervals(item : T) : Set[Interval] = reverseMap(item)
}

object IntervalMap {
  def apply[T]() : IntervalMap[T] = IntervalMap(Map())
}


