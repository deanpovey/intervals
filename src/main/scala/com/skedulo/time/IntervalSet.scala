package com.skedulo.time

/**
  * An IntervalSet is an immutable data structure that holds a number of (possibly overlapping) Intervals.  Unlike the
  * methods in Intervals, this class will not merge individual intervals when performing set-theoretic functions.
  * This allows it to be used it to manipulate
  * keys in an IntervalMap.
  *
  */
trait IntervalSet extends Iterable[Interval] {

  lazy val flattened = Intervals.flatten(this)

  /**
   * Return all the intervals overlapping the specified one.
   *
   * @param interval Interval to check for overlaps
   * @return intervals that overlap
   */
  def overlapping(interval: Interval): IntervalSet = findAll(interval, interval.overlaps)

  /**
    * Find the self-intersecting intervals in this interval set.
    *
    * @return intervals in this IntervalSet that overlap any other interval in this IntervalSet
    */
  def selfIntersection() : Iterable[Interval] = intersection(Intervals.selfIntersection(this))


  /**
    * Return true if this IntervalSet is self-intersecting.
    * @return True if the IntervalSet is self intersecting false otherise
    */
  def selfIntersecting() : Boolean = Intervals.selfIntersecting(this)

  /**
    * Find all the intervals that intersect the supplied interval.
    *
    * @param interval intervals to check
    *
    * @return
    */
  def intersection(interval: Interval): IntervalSet = findAll(interval, interval.intersects)

  /**
    * Find the intervals in this IntervalSet that intersect any of the supplied intervals.
    * @param intervals intervals to check for intersections
    * @return intervals in this IntervalSet that overlap the supplied intervals.
    */
  def intersection(intervals: Iterable[Interval]): Iterable[Interval]

  /**
    * Union of this intervalSet with the given IntervalSet.
    * @param intervalSet IntervalSet to find union for
    * @return intervals in either this or that IntervalSet.
    */
  def union(intervalSet: IntervalSet): IntervalSet

  /**
    * Union of this intervalSet with the given intervals.
    * @param intervals Intervals to find union for
    * @return intervals in either this or that IntervalSet.
    */
  def union(intervals: Iterable[Interval]): IntervalSet

  /**
    * Find all intervals included by the search interval that match the given predicate.
    * @param searchInterval the interval to limit the search
    * @param predicate predicate to match
    * @return intervals matched
    */
  def findAll(searchInterval : Interval, predicate: Interval => Boolean) : IntervalSet

  /**
    * Find the first interval included by the search interval that matches the givn predicate.
    * @param searchInterval the interval to limit the search
    * @param predicate predicate to match
    * @return optional result if an interval is found
    */
  def findAny(searchInterval : Interval, predicate: Interval => Boolean) : Option[Interval]

  /**
    * Partition intervals into those before the given point, those including it and those after it
    * @param t point to partition by
    * @return ({ x | x < t}, {y | y includes t}, {z | z > t})
    */
  def partition(t: Long) : (Iterable[Interval], Iterable[Interval], Iterable[Interval])

  /**
    * Partition intervals into those before the given interval, those intersecting it and those after it
    * @param interval point to partition by
    * @return ({ x | x < t}, {y | y intersects t}, {z | z > t})
    */
  def partition(interval : Interval) : (Iterable[Interval], Iterable[Interval], Iterable[Interval])

  /**
    * Return true if the given interval is included in the IntervalSet.  For example if the IntervalSet contains
    * (1, 10), (20, 30) then this will return true for the intervals (1, 10), (2, 5), (20, 20), but not for the
    * intervals (10, 12) nor (40, 50)
    *
    * @param interval interval to determine inclusion
    * @return true if interval is included false otherwise
    */
  def includes(interval : Interval) : Boolean

  /**
    * Return the interval that includes all the intervals in this IntervalSet
    * @return the span of this IntervalSet or None if the IntervalSet is empty
    */
  def span : Option[Interval]

  /**
    * Add an interval to this IntervalSet
    * @param interval interval to add
    * @return new IntervalSet containing the interval
    */
  def +(interval : Interval) : IntervalSet

  /**
    * Remove an interval from this IntervalSet
    * @param interval interval to remove
    * @return new IntervalSet with the element removed or the current set if it was not included.
    */
  def -(interval : Interval) : IntervalSet

  /**
    * Returns true if this IntervalSet includes an exact interval matching the supplied Interval.
    * @param interval Interval to match
    * @return true if this set contains the Interval
    */
  def contains(interval: Interval) : Boolean

  /**
    * Return an Empty IntervalSet
    * @return Empty IntervalSet
    */
  def empty : IntervalSet

  /**
    * Return true if the IntervalSet is empty
    * @return true if the IntervalSet is empty
    */
  def isEmpty : Boolean

  def flatten : Iterable[Interval] = flattened

}

/**
  * This implements an IntervalSet as an Immutable IntervalTree. A tree consists of either IntervalTreeNodes or
  * EmptyIntervalTrees.
  *
  * <p>The Tree is a recursive data structure and we take care to retain unchanged portions when creating new
  * IntervalTrees via the add and subtract methods.</p>
  *
  * <p>NB: This data structure is somewhat naive, it is unbalanced so can degrade to O(n<sup>2</sup>) operations in the
  * worse case.  Some of the traversal methods are not tail-recursive, although this does not seem to be a
  * problem in practice.  It should be sufficient for a prototype</p>
  */
sealed trait IntervalTree extends IntervalSet {

  override def empty: IntervalSet = EmptyIntervalTree

  override def isEmpty: Boolean = this match {
    case EmptyIntervalTree => true
    case _ : IntervalTreeNode => false
  }

  override def union(intervalSet: IntervalSet): IntervalSet = this match {
    case EmptyIntervalTree => {
      intervalSet
    }

    case IntervalTreeNode(centre, span, left, overlapped, right) ⇒
      Intervals.partition(centre, intervalSet) match {
        case (a, b, c) if a.isEmpty && b.isEmpty && c.isEmpty => {
          this
        }
        case (newLeft, newOverlapped, newRight) ⇒ {
          IntervalTreeNode(centre, span, left union newLeft, overlapped ++ newOverlapped, right union newRight)
        }
      }
  }

  override def union(intervals: Iterable[Interval]): IntervalSet = {
    intervals match {
      case intervals if intervals.isEmpty => this
      case intervalSet: IntervalSet ⇒ this union intervalSet
      case _ ⇒
        this match {
          case EmptyIntervalTree ⇒ IntervalSet(intervals)
          case IntervalTreeNode(centre, span, left, overlapped, right) ⇒
            val (newLeft, newOverlapped, newRight) = Intervals.partition(centre, intervals)
            IntervalTreeNode(centre, span, IntervalSet(left ++ newLeft), overlapped ++ newOverlapped, IntervalSet(right ++ newRight))
        }
    }
  }

  override def includes(elem: Interval): Boolean = findAny(elem, _ includes elem).isDefined

  override def contains(elem: Interval): Boolean = findAny(elem, _ equals elem).isDefined

  override def equals(obj: scala.Any): Boolean = obj match {
    case that : IntervalSet => this.toSet == that.toSet
    case _                  => false
  }

}

object IntervalSet {
  def apply(): IntervalSet = EmptyIntervalTree
  def apply(interval: Interval*): IntervalSet = apply(interval.toList)
  def apply[A <: Iterable[Interval]](intervals: A): IntervalSet = intervals match {
    case intervalSet: IntervalSet => intervalSet
    case empty if empty.isEmpty   => EmptyIntervalTree
    case _                        => IntervalTreeNode(intervals)
  }
  def apply(start: Long, end: Long) : IntervalSet = IntervalSet(Interval(start, end))
}

case object EmptyIntervalTree extends IntervalTree {
  override def overlapping(interval: Interval) : IntervalSet = EmptyIntervalTree

  override def iterator = Iterator()

  override def findAll(interval: Interval, predicate: (Interval) => Boolean): IntervalSet = EmptyIntervalTree

  override def findAny(interval: Interval, predicate: (Interval) => Boolean): Option[Interval] = None

  override def span : Option[Interval] = None

  override def +(elem: Interval): IntervalSet = IntervalSet(elem)

  override def -(elem: Interval): IntervalSet = EmptyIntervalTree

  override def intersection(that: Iterable[Interval]) : Iterable[Interval] = Nil

  override def partition(instant: Long) = (Nil, Nil, Nil)

  override def partition(interval: Interval) = (Nil, Nil, Nil)
}

case class IntervalTreeNode(centre: Long, totalSpan: Interval, left: IntervalSet, overlapped: Set[Interval], right: IntervalSet) extends IntervalTree {

  def span : Option[Interval] = Some(totalSpan)

  lazy val localSpan : Interval = Intervals.span(overlapped).getOrElse {Instant(centre)}

  override def findAll(overlappingInterval : Interval, predicate: (Interval) => Boolean) : IntervalSet = {
    if (!predicate(totalSpan)) IntervalSet()
    else {
      val result = overlapped.filter(predicate) ++
        (if (!overlappingInterval.after(centre)) left.findAll(overlappingInterval, predicate) else EmptyIntervalTree) ++
        (if (!overlappingInterval.before(centre)) right.findAll(overlappingInterval, predicate) else EmptyIntervalTree)
      IntervalSet(result)
    }
  }

  override def findAny(overlappingInterval : Interval, predicate: (Interval) => Boolean) : Option[Interval] = {
    if (!predicate(totalSpan)) None
    else {
      overlapped.find(predicate) orElse {
        findLeft(overlappingInterval, predicate)
      } orElse {
        findRight(overlappingInterval, predicate)
      }
    }
  }

  private def findLeft(overlappingInterval : Interval, predicate: Interval => Boolean) : Option[Interval] = {
    if (overlappingInterval.after(centre)) None
    else left.findAny(overlappingInterval, predicate)
  }

  private def findRight(overlappingInterval : Interval, predicate: Interval => Boolean) : Option[Interval] = {
    if (overlappingInterval.before(centre)) None
    else right.findAny(overlappingInterval, predicate)
  }

  override def partition(instant: Long) : (Iterable[Interval], Iterable[Interval], Iterable[Interval]) = {
    if (!totalSpan.includes(instant)) {
      if (totalSpan.before(instant)) (this, Nil, Nil)
      else (Nil, Nil, this)
    } else {
      val leftPartition = left.partition(instant)
      val overlappingPartition = Intervals.partition(instant, overlapped)
      val rightPartition = right.partition(instant)
      (leftPartition._1 ++ overlappingPartition._1 ++ rightPartition._1,
        leftPartition._2 ++ overlappingPartition._2 ++ rightPartition._2,
        leftPartition._3 ++ overlappingPartition._3 ++ rightPartition._3)
    }
  }

  override def partition(interval: Interval) : (Iterable[Interval], Iterable[Interval], Iterable[Interval]) = {
    if (!totalSpan.overlaps(interval)) {
      if (totalSpan.before(interval.start)) (this, Nil, Nil)
      else (Nil, Nil, this)
    } else {
      val leftPartition = left.partition(interval)
      val overlappingPartition = Intervals.partition(interval, overlapped)
      val rightPartition = right.partition(interval)
      (leftPartition._1 ++ overlappingPartition._1 ++ rightPartition._1,
        leftPartition._2 ++ overlappingPartition._2 ++ rightPartition._2,
        leftPartition._3 ++ overlappingPartition._3 ++ rightPartition._3)
    }
  }


  override def intersection(intervals: Iterable[Interval]) : Iterable[Interval] = {
    Intervals.span(intervals) match {
      case None => Nil
      case Some(interval) if !interval.overlaps(totalSpan) => Nil
      case _ => {
        val (inLeft, centre, inRight) = Intervals.partition(localSpan, intervals)
        val leftOverlaps = left.intersection(inLeft)
        val centreOverlaps = centre.flatMap(i => this.overlapping(i))
        val rightOverlaps = right.intersection(inRight)
        (leftOverlaps ++ centreOverlaps) ++ rightOverlaps
      }
    }
  }

  override def iterator: Iterator[Interval] = new Iterator[Interval]() {
    lazy val left : Iterator[Interval] = IntervalTreeNode.this.left.iterator
    lazy val middle : Iterator[Interval] = IntervalTreeNode.this.overlapped.iterator
    lazy val right : Iterator[Interval] = IntervalTreeNode.this.right.iterator

    override def hasNext: Boolean = left.hasNext || middle.hasNext || right.hasNext

    override def next(): Interval = {
      if (left.hasNext) left.next()
      else if (middle.hasNext) middle.next()
      else if (right.hasNext) right.next()
      else throw new NoSuchElementException
    }
  }

  override def +(elem: Interval): IntervalSet = {
    val newSpan = totalSpan.span(elem)
    if (elem.intersects(centre)) new IntervalTreeNode(centre, newSpan, left, overlapped + elem, right)
    else if (elem.before(centre)) new IntervalTreeNode(centre, newSpan, left + elem, overlapped, right)
    else new IntervalTreeNode(centre, newSpan, left, overlapped, right + elem)
  }

  override def -(elem: Interval): IntervalSet = {
    if (!totalSpan.includes(elem)) this
    else {
      val (newLeft, newOverlapped, newRight) =
        if (elem.intersects(centre)) (left, overlapped - elem, right)
        else if (elem.before(centre)) (left - elem, overlapped, right)
        else (left, overlapped, right - elem)

      val span = Seq(newLeft.span, Intervals.span(newOverlapped), newRight.span).flatten.reduceOption(_ span _)

      span match {
        case None => EmptyIntervalTree
        case Some(newSpan) => IntervalTreeNode(centre, newSpan, newLeft, newOverlapped, newRight)

      }
    }
  }
}

object IntervalTreeNode {
  def apply(intervals: Iterable[Interval]): IntervalTreeNode = {
    require(intervals.nonEmpty)
    val span = Intervals.span(intervals).get
    val centre: Long = span.centre
    val (left, overlapping, right) = Intervals.partition(centre, intervals)
    new IntervalTreeNode(centre, span, IntervalSet(left), overlapping.toSet, IntervalSet(right))
  }
}
