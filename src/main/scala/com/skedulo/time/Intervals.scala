package com.skedulo.time

import scala.annotation.tailrec
import scala.collection.immutable.Nil

/**
  * Methods that deal with multiple intervals
  */
object Intervals {

  /**
    * Return the minimum Interval that includes all the given intervals.
    * @param intervals Intervals to find span for
    * @return the minimum Interval that includes all the given intervals
    */
  def span(intervals: Iterable[Interval]): Option[Interval] = intervals match {
    case intervalSet : IntervalSet => intervalSet.span
    case _ =>  intervals.reduceLeftOption(_ span _)
  }

  /**
    * Compute the set of intervals that include all self intersecting intervals.
    * @param intervals Intervals to find self-intersections for
    * @return self-intersecting intervals or empty if no intervals self intersect.
    */
  def selfIntersection(intervals : Iterable[Interval]) : Iterable[Interval] = {

    def fromStartToMatchingEnd(sorted: Seq[IntervalEndPoint], accum: List[Interval]) : Seq[Interval] = {
      sorted match {
        case Nil => accum
        case Start(_) :: End(_) :: t => fromStartToMatchingEnd(t, accum)
        case Start(start) :: t => skipUntilMatchingEnd(start, t, 1, accum)
      }
    }

    def skipUntilMatchingEnd(start : Long, rest : Seq[IntervalEndPoint], endRequired : Int, accum: List[Interval]) : Seq[Interval] = {
      rest match {
        case Start(_) :: t => skipUntilMatchingEnd(start, t, endRequired + 1, accum)
        case End(end) :: t =>
          if (endRequired == 1) fromStartToMatchingEnd(t, accum :+ Interval(start, end))
          else skipUntilMatchingEnd(start, t, endRequired - 1, accum)
      }
    }

    val endPoints = intervals.flatMap(_.flatten)
    //val sorted = endPoints.toSeq.sorted { (a : IntervalEndPoint, b : IntervalEndPoint) => a.value.compareTo(b.value) }
    val sorted = endPoints.toSeq.sorted
    fromStartToMatchingEnd(sorted, Nil)
  }

  /**
    * Returns true if the given intervals contain self-intersections
    * @param intervals intervals to consider
    * @return true if any intervals in the given set are self intersecting
    */
  def selfIntersecting(intervals : Iterable[Interval]) : Boolean = {

    @tailrec
    def findSelfIntersections(sorted: Seq[IntervalEndPoint]) : Boolean = {
      sorted match {
        case Seq() => false
        case Start(_) +: End(_) +: t => findSelfIntersections(t)
        case Start(_) +: _ => true
      }
    }

    val endPoints = intervals.flatMap(_.flatten)
    val sorted = endPoints.toList.sorted { (a : IntervalEndPoint, b : IntervalEndPoint) => a.value.compareTo(b.value) }
    findSelfIntersections(sorted)
  }

  /**
    * Merge intersecting intervals so that the result intervals are disjoint.
    * @param intervals intervals to merge
    * @return sorted set of flattened intervals
    */
  def flatten(intervals : Iterable[Interval]) : Iterable[Interval] = {

    def fromStartToMatchingEnd(sorted: Seq[IntervalEndPoint]) = {
      sorted match {
        case Nil => Nil
        case Start(start) :: t => {
          skipUntilMatchingEnd(start, t, 1)
        }
      }
    }

    def fromPossiblyAdjacentStartToMatchingEnd(start : Long, lastEnd : Long, sorted: Seq[IntervalEndPoint]) = {
      sorted match {
        case Nil => List(Interval(start, lastEnd))
        case Start(nextStart) :: t if nextStart == lastEnd => skipUntilMatchingEnd(start, t, 1)
          skipUntilMatchingEnd(start, t, 1)
        case _ => Interval(start, lastEnd) +: fromStartToMatchingEnd(sorted)
      }
    }

    def skipUntilMatchingEnd(start : Long, rest : Seq[IntervalEndPoint], endRequired : Int) : Seq[Interval] = {
      rest match {
        case Start(_) :: t => skipUntilMatchingEnd(start, t, endRequired + 1)
        case End(end) :: t =>
          if (endRequired == 1) fromPossiblyAdjacentStartToMatchingEnd(start, end, t)
          else skipUntilMatchingEnd(start, t, endRequired - 1)
      }
    }

    val endPoints = intervals.flatMap(_.flatten)
    val sorted = endPoints.toList.sorted { (a : IntervalEndPoint, b : IntervalEndPoint) => a.value.compareTo(b.value) }
    fromStartToMatchingEnd(sorted)
  }

  /**
    * Convert a list of Intervals to a list of IntervalEndPoint
    * @param intervals intervals to convert
    * @return list of interval endponts
    */
  def endpoints(intervals : Iterable[Interval]) : Iterable[IntervalEndPoint] = {
    intervals.flatMap(_.flatten)
  }

  /**
    * Relative difference of two sets of Intervals
    * @param a first set of intervals
    * @param b second set of intervals
    * @return Intervals in a not in b
    */
  def diff(a: Iterable[Interval], b: Iterable[Interval]) = merge(a, b, (inA, inB) => inA && !inB)

  /**
    * Union of two sets of Intervals
    * @param a first set of intervals
    * @param b second set of intervals
    * @return Intervals in either a or b
    */
  def union(a: Iterable[Interval], b: Iterable[Interval]) = merge(a, b, (inA, inB) => inA || inB)

  /**
    * Intersection of two sets of Intervals
    * @param a first set of intervals
    * @param b second set of intervals
    * @return Intervals in both a and b
    */
  def intersection(a: Iterable[Interval], b: Iterable[Interval]) = merge(a, b, (inA, inB) => inA && inB)

  /**
    * Symmetric difference of two sets of Intervals
    * @param a first set of intervals
    * @param b second set of intervals
    * @return Intervals in a or b but not both
    */
  def symmetricDiff(a: Iterable[Interval], b: Iterable[Interval]) = merge(a, b, (inA, inB) => inA ^ inB)

  /**
    * Merge two lots of Intervals according to the inclusion predicate.
    *
    * @param a first list of intervals
    * @param b second set of intervals
    *
    * @param inclusionPredicate a function which is given two boolean parameters indicating whether a given instant
    *                           is contained in the first or second set of intervals respectively
    * @return the intervals merged according to the inclusion predicate
    */
  def merge(a: Iterable[Interval], b: Iterable[Interval], inclusionPredicate: (Boolean, Boolean) => Boolean) : Iterable[Interval] = {

    def isOdd(n : Int) : Boolean = n % 2 != 0

    // Whether the given sequence of intervals is open or closed (ie. if the last point is the start or end of an interval)
    def isOpen(seq : Seq[Long]) : Boolean = isOdd(seq.size)

    def isIn(current : Long, endPoints : Vector[Long], i : Int) =
    // This takes a little explaining.  We are trying to determine whether 'current' is in the interval enclosed by
    // the endpoint indexed by 'i' in 'endpoints'.  The strategy is to determine when the interval is not enclosed
    // and negate it.  There are two cases to consider, either:
    // 1. endPoint(i) is the start of an interval (i is even).  We do not enclose this interval if current < endPoint(i)
    // 2. endPoint(i) is the end of an interval (i is odd). We do not enclose this interval if current is >= endPoint(i)
    //
    // As a boolean expression: !((current < endPoints(i) && !isOdd(i)) || !(current < endPoints(i) && isOdd(i)))
    //
    // The non-negated version is of the form: !a ^ b v a ^ !b = a xor b
    //
    // And hence can be rewritten as:
      !(current < endPoints(i) ^ isOdd(i))

    val aEndPoints = endpoints(flatten(a)).map(_.value).toVector
    val bEndPoints = endpoints(flatten(b)).map(_.value).toVector

    var res: List[Long] = Nil
    var aIndex = 0
    var bIndex = 0

    while (aIndex < aEndPoints.size && bIndex < bEndPoints.size) {
      val current = Math.min(aEndPoints(aIndex), bEndPoints(bIndex))

      val inA = isIn(current, aEndPoints, aIndex)

      val inB = isIn(current, bEndPoints, bIndex)

      // Add the point if its included in the result and the result is currently open. If the point is not included
      // and the result is open then close it.
      if (inclusionPredicate(inA, inB) ^ isOpen(res)) res = res :+ current

      if (current == aEndPoints(aIndex)) aIndex = aIndex + 1
      if (current == bEndPoints(bIndex)) bIndex = bIndex + 1
    }

    // Append any left over points depending on the inclusion predicate
    if (aIndex < aEndPoints.size && inclusionPredicate(true, false)) res = res ++ aEndPoints.drop(aIndex)
    else if (bIndex < bEndPoints.size && inclusionPredicate(false, true)) res = res ++ bEndPoints.drop(bIndex)

    res.sliding(2, 2).map(l => Interval(l.head, l.last)).toList
  }

  /**
    * Partition intervals into those before the given point, those including it and those after it
    * @param t point to partition by
    * @param intervals intervals to partition
    * @tparam A type of Intervals
    * @return ({ x | x < t}, {y | y intersects t}, {z | z > t})
    */
  def partition[A <: Iterable[Interval]](t: Long, intervals: A): (Iterable[Interval], Iterable[Interval], Iterable[Interval]) = {
    intervals match {
      case intervalSet : IntervalSet => intervalSet.partition(t)
      case _ =>
        val (overlapping, nonOverlapping) = intervals.partition(_.intersects(t))
        val (leftIntervals, rightIntervals) = nonOverlapping.partition(_.before(t))
        (leftIntervals, overlapping.toSet, rightIntervals)
    }

  }

  /**
    * Partition intervals into those before the given point, those intersecting it and those after it
    * @param interval interval to partition by
    * @param intervals intervals to partition
    * @tparam A type of Intervals
    * @return ({ x | x < t}, {y | y intersects t}, {z | z > t})
    */
  def partition[A <: Iterable[Interval]](interval: Interval, intervals: A): (Iterable[Interval], Iterable[Interval], Iterable[Interval]) = {
    intervals match {
      case intervalSet: IntervalSet => intervalSet.partition(interval)
      case _ =>
        val (intersecting, disjoint) = intervals.partition(i => i.intersects(interval))
        val (leftIntervals, rightIntervals) = disjoint.partition(_.end <= interval.start)
        (leftIntervals, intersecting.toSet, rightIntervals)
    }
  }
}

