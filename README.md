Intervals Prototype
===================

Background
----------
The Mastermind / Plan Service relies heavily on the processing of intervals and interval sets to
determine scheduling of resources.  These are sets of possibly overlapping regions of time which
may represent for example:

- Availability
- Scheduled time
- Travel time

This prototype implements a more robust set-theoretic approach to using intervals, and sets of
intervals can be efficiently searched and combined.

Intervals
---------
An `Interval` is defined as an open-closed range consisting of a start and end time, and encompassing
all time instants from the start, up to, but not including the end time, ie:

``Interval(start, end) = { x | start <= x < end }``

The length of the interval may be defined by;

``Interval(start, end).length = end - start``

It is possible to support instants of time by using intervals of zero length, e.g.

``Interval(instant, instant).length == 0``

It is also possible to support open or infinite intervals by allowing either or both the start and
end to use a value that represents the earliest or latest possible value, e.g.:

- All of time = (-∞, +∞)
- After 10 = (10, +∞)
- Before 10 = (-∞, 10)

Lastly, predicates are supported to order an instant or interval as 

- a before b - if a.end <= b.start
- a after b - if a.start >= b.end

`IntervalSet`
-----------
An `IntervalSet` is defined as a collection of 0 or more Intervals. An `IntervalSet` may be disjoint
in which case none of the the intervals are intersecting, or it may be self-intersecting.
An `Interval` is equivalent to the `IntervalSet` containing only that interval.

### Self-Intersections
`IntervalSet` contains a unary `selfIntersection` operator and the corresponding 
`selfIntersecting` predicate.  This `selfIntersection` operation returns all pairs of items in
the `IntervalSet` for which the `intersect`s predicate is true (see below), while the `selfIntersects`
predicate returns true if `selfIntersection` is not the empty set.

### Partition
`IntervalSet` has a partition operation that takes either an `Interval` or an Instant and partitions
the `IntervalSet` into 3 sets such that:

``IntervalSet partition Interval = ({x | x before interval}, {y | y overlaps interval}, {z | z after interval})``
 
### Span
The `span` of an `IntervalSet` is the smallest `Interval` that includes all items in the set.
Because empty intervals are not defined it is undefined on the empty set.

Boolean Predicates
------------------
The following binary boolean predicates can be used to determine relationships between two intervals
or interval sets.

- `a intersects b` - if a and b have any point apart from their end point in common.  e.g.
 `(1, 10) intersects (5, 20)` but not `(10, 20)` or `(20, 30)`
- `a touches b` - if a and b only contain an end point in common. e.g. `(1, 10) touches
 (10, 20)` but not `(5, 20)` or `(20, 30)`
- `a overlaps b` - `a intersects b` or `a touches b`
- `a includes b` - if all the points of b are inside the interval of b. e.g. `(1, 10) includes (5, 6),
(1, 5), (5, 10)` and itself, but not `(1, 11)` or `(20, 30)`
- `a includedBy b` - if `b includes a`
- `a disjoint b` - if no point of a is inside the interval of b and vice versa. This is the inverse
of `a intersects b`. e.g. `(1, 10) disjoint (10, 20)` and `(20, 30)`, but not `(1, 5), (9, 20)`.

These predicates are also defined for an instant by considering the `Interval` `(instant, instant)`.

Set-Theoretic Functions
-----------------------
The following binary set-theoretic functions are defined for pairs of Intervals or `IntervalSet`s a and b with an `IntervalSet` as the output:

- `a union b` - An `IntervalSet` containing all of the points in a and b.  e.g. (1, 10) union (10, 20) = {(1, 20)};  (1, 10) union (20, 30) = {(1, 10), (20, 30)}
- `a intersection b` - An `IntervalSet` containing all the points in both a and b. e.g. (1, 10) intersection (5, 20) = {(5, 10)},;(1, 10) intersection (20, 30) = {}
- `a diff b` - All the items in a that are not in b (also known as the relative complement). e.g. (1, 10) diff (5, 20) = {(1, 5)};  (1, 10) diff (5, 7) = {(1, 5), (7, 10)}
- `a symmetricDiff b` - (a diff b) union (b diff a)

The unary function `~a` (complement of a) is defined as all the point not in a, which is equivalent to `a diff (-∞, +∞)`

Implementation details
----------------------
The following describes the data structures and algorithms which are used to represent the above
abstract types.

### IntervalTree

An `IntervalTree` implements fast searching (O(n log n)) of an `IntervalSet` by recursively
partitioning the `Interval`s based on an arbitrarily chosen point (the centre).  There are
balanced variations or an existing balanced binary tree may be augmented to implement an
`IntervalTree`.  Central to the implementation is the partition operation described above.
The `IntervalTree` is mainly used to implement the binary predicates by short-circuiting the search.
It does this by storing at each node the span of the overlapping members.   It can then decide
whether to descend the left, right or check the overlapping methods according to whether the
supplied interval intersects with this span.

### Flatten

The set-theoretic operations require that the `IntervalSet` is not self-intersecting.  To do this
it first should be flattened to a disjoint set.  This is done by sorting the start and end points
by value and then matching pairs of start and end points.  This can be visualised by replacing start
and end points with open an close brackets.  We maintain a counter which is incremented every time
we see an open bracket and decremented for a close bracket.  When the counter is 1 we write an open
bracket (start) to the output, and when it is 0 we write a close bracket (finish)

    ___(..(...)....)___(.)____(..(..)...(....))____
       1  2   1    0   1 0    1  2  1   2    10
    ___(...........)___(.)____(...............)____```

The resulting interval set is flattened.  This is an O(n) operation, but requires sorting of the
initial endpoints which is O(n log n).

### Self Intersecting

A variation of the Flatten technique can be used to determine if self-intersecting intervals exist
and what the `span` of those intervals are.  This is as follows.  As before sort by `Interval`
endpoints retaining whether they are a start or end point.  Scan the list incrementing or
decrementing the accumulator. However, this time discard intervals where a `Start` and `End` are
adjacent.  Only retain flattened intervals that start with more than 1 start, e.g.

    ___(..(...)....)___(.)____(..(..)...(....))____
       1  2   1    0   1 0    1  2  1   2    10
    ___(...........)__________(...............)____
    
The returned intervals include the self-intersecting intervals.

### Merge operations
Merge operations are those operations that combine two `Interval` sets on the basis of a
membership predicate.

The membership predicate takes two boolean parameters: whether a point is a member of a and/or b
respectively and returns whether it should be a member of the resulting set.

This allows us to implement the set-theoretic operations in terms of a merge, ie.

    / a diff b -> Intervals in a not in b
    def diff(a : IntervalSet, b : IntervalSet) : IntervalSet = merge(a, b, (inA, inB) => inA && !inB)

    // a union b -> Intervals in a or b
    def union(a : IntervalSet, b : IntervalSet) : IntervalSet = merge(a, b, (inA, inB) => inA || inB)

    // a intersection b -> Intervals in either a or b
    def intersection(a : IntervalSet, b : IntervalSet) : IntervalSet = merge(a, b, (inA, inB) => inA && inB)

    // a symmetricDef b -> Intervals in either set but not in both
    def symmetricDiff(a : IntervalSet, b : IntervalSet) : IntervalSet = merge(a, b, (inA, inB) => inA ^ inB

The merge operation itself works by iterating over the endpoints of the (non-overlapping) intervals
in a and b.  It takes the minimum unvisited endpoint and then determine whether this point is in
both a or b.  Using the resulting membership predicate it will then determine if the point should be
included in the result set.  Even if the item is included in the result set it is only appended if
the current result does not end with a closed interval, as otherwise the result set would contain
overlapping results.

The following example tries to show how this works for the given operations

        a : ___(...........)__________(...............)____
        b : (...)______(.......)_____(...)__________(..)_()
      inA : N..YY......Y...N...N_____NY..Y..........Y.NN.NN
      inB : Y..YN......Y...Y...N_____YY..N..........Y.YN.YN
    diff  :    (......)                 (..........)
    union : (..................)     (.................) ()
    inter :   ()      (...)          (..)          (.)
    symdf : (..)(......)  (....)     ()  (..........) () ()
    
IntervalMap
-----------

An extension of the `IntervalSet` is the `IntervalMap[T]` type which is multi-bidirectional map
which allows associating an `Interval` with one or more items of type T.  For example supposing there
is a type Job corresponding to a Job.  `IntervalMap[Job]` can provide:

- Ability to lookup Jobs by Interval
- Ability to lookup `Interval` by Job
- Ability to extract and perform operations on the Intervals mapped to Jobs as an `IntervalSet`

The usefulness of this structure will become apparent when we describe the use cases below.

Use Cases
---------
To illustrate how these structures and functions can be used to efficiently and succinctly implement
common functions in Mastermind consider the following use cases.

### Finding gaps in a Job Schedule

Finding gaps is the same as finding the difference of the `IntervalSet` mapping to jobs with the
interval we want to find gaps for, e.g.

    def findGaps(interval : Interval, jobs : IntervalMap[Job]) : IntervalSet = {
      IntervalSet(interval) diff jobs.toIntervalSet()
    }

It can also be seen that it would be trivial to find gaps over multiple intervals using this
technique.  This idea can be extended to providing a findGaps function that uses both an IntervalMap
for Jobs and an `IntervalSet` describing when a resource is available.

### Finding gaps in a Job Schedule with availability

    def findGaps(interval : Interval, jobs : IntervalMap[Job], availability : IntervalSet) : IntervalSet = {
      (availability intersection interval) diff jobs.toIntervalSet
    }

### Finding scheduling conflicts
Scheduling conflicts are equivalent to self-intersecting intervals in the Job IntervalMap

    def findSchedulingConflicts(jobs : IntervalMap[Job]) : Iterable[Job] = {
      jobs.toIntervalSet().selfIntersecting().flatMap(jobs.toMap()(_))
    }

This finds the self-intersecting intervals and then converts these to a list of jobs referenced by
those intervals.

### Arbitrary Job Rules

Providing arbitrary decisions about whether a resource can be used for certain jobs is as simple as
producing a function that produces an unavailability `IntervalSet` and finding the difference between
this and the availability `IntervalSet`.  For example.  Suppose we have a rule that you cannot do
jobs of type A within 24 hours of type B.  The following pseduocode illustrates this rule:

    val cantDoJobsOfTypeA : IntervalSet =
        jobs.filter(_.jobType == A)
            .toIntervalSet()
            .map { case Interval(start, finish) => Interval(start - 24h, end + 24h) }
    val availability = ...
    val gaps = findGaps(interval, jobs, availability diff cantDoJobsOfTypeA)

Performance
-----------
Based on 1 million jobs a prototype implementation of these use cases took the following timings on
a MacBook Pro 2016

    | Use Case	                | Timing |
    | findSchedulingConflicts	| ~30ms  |
    | findGaps	                | ~230ms |

Toward a comprehensive model of Job scheduling based on `IntervalSet`/Map operations
------------------------------------------------------------------------------------

We have seen how by producing an efficient model of `IntervalSet`s and set-theoretic operations and
associating these intervals with jobs or other types makes it simple to combine these structures
to produce complex scheduling policies.  The following outlines a framework which could be
formalised to make this an extensible model:
 
FindGaps can be generalised to allow injection of custom rules that produce available time or
Unavailable time `IntervalSet`s and combine these to produce an availability map

                Available Time
                      |                            
                 intersection
                      |
                      v
           (Custom Availability Rules)
                      |
                  difference
                      |
                      v
    Unavailable Time union (Custom Unavailability Rules)
                      |
                      V
                 availability        

We can then find the difference of this with the current job schedule to findGaps.   The availability
may be context dependent, ie. the custom rules may return different results depending on our
previous job history and the current job being requested.

We can use the intersection of the availability `IntervalSet` with the schedule to find conflicts
where we are not available.  This may require some filtering of the jobs if we have used a custom
rule.  For example if using the unavailability rule for Jobs of type A after type B we filter on
Jobs of type B when checking the schedule.  We can actually partition the jobs any number of ways
for these types of checks as long as the same membership rules to determine custom availability
are used to determine how the job schedule is checked.